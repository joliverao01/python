# coding=utf-8
import pygame
pygame.init()

walkRight = [pygame.image.load('img/R%.png'% frame) for frame in range(1,9)]
walkLeft = [pygame.image.load('img/L1.png'), pygame.image.load('img/L2.png'), pygame.image.load('img/L3.png'), pygame.image.load('img/L4.png'), pygame.image.load('img/L5.png'), pygame.image.load('img/L6.png'), pygame.image.load('img/L7.png'), pygame.image.load('img/L8.png'), pygame.image.load('img/L9.png'), pygame.image.load('img/L10.png'), pygame.image.load('img/L11.png')]

bg = pygame.image.load('img/bg.jpg')
char = pygame.image.load('img/standing.png')

clock = pygame.time.Clock()

windowwidth = 500
windowheight = 500
x = 50
y = 50
pjwidth = 40
pjheight = 60
vel = 5
isJump = False
jumpCount = 10
left = False
right = False
walkCount = 0
run = True

window = pygame.display.set_mode((windowwidth,windowheight)) #Define el tamaño de la pantalla

pygame.display.set_caption("First Game") #Define el nombre del programa

def redrawWindow():
    global walkCount
    window.blit(bg, (0,0))
    if walkCount + 1 >= 27:
        walkCount = 0
        if left:
            win.blit(walkLeft[walkCount//3])
            walkCount += 1
        elif right:
            win.blit(walkRight[walkCount//3], (x,y))
            walkCount += 1
        else:
            win.blit(char, (x,y))

    pygame.display.update()

while run: #Comienza el bucle
    clock.tick(27)

    for event in pygame.event.get(): #Comienza otro bucle
        if event.type == pygame.QUIT:
            run = False

    keys = pygame.key.get_pressed()

    if keys[pygame.K_LEFT] and x > vel:
        x -= vel
        left = True
        right = False
    elif keys[pygame.K_RIGHT] and x < windowwidth - pjwidth - vel:
        x += vel
        left = False
        right = True
    else:
        right = False
        true = False
        walkCount = 0
    if not(isJump):
        if keys[pygame.K_UP] and y > vel:
            y -= vel
        if keys[pygame.K_DOWN] and y < windowwidth - pjheight - vel:
            y += vel
        if keys[pygame.K_SPACE]:
            isJump = True
            right = False
            left = False
    else:
        if jumpCount >= -10:
            neg = 1
            if jumpCount < 0:
                neg = -1
            y -= (jumpCount ** 2) * 0.2 * neg # Para que el salto sea menor, se modifica el 0.?
            jumpCount -= 1
        else:
            isJump = False
            jumpCount = 10

    redrawWindow()


pygame.quit()


#
